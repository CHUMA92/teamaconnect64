#include "../TeamAConnect64.h"

namespace CLIForm {

#pragma region Windows Form Designer generated code
	/// <summary>
	/// Required method for Designer support - do not modify
	/// the contents of this method with the code editor.
	/// </summary>
	void MyForm::InitializeComponent(void)
	{
		this->components = (gcnew System::ComponentModel::Container());
		this->button1 = (gcnew System::Windows::Forms::Button());
		this->button2 = (gcnew System::Windows::Forms::Button());
		this->button3 = (gcnew System::Windows::Forms::Button());
		this->button4 = (gcnew System::Windows::Forms::Button());
		this->button5 = (gcnew System::Windows::Forms::Button());
		this->button6 = (gcnew System::Windows::Forms::Button());
		this->button7 = (gcnew System::Windows::Forms::Button());
		this->button8 = (gcnew System::Windows::Forms::Button());
		this->button9 = (gcnew System::Windows::Forms::Button());
		this->button10 = (gcnew System::Windows::Forms::Button());
		this->button11 = (gcnew System::Windows::Forms::Button());
		this->button12 = (gcnew System::Windows::Forms::Button());
		this->button13 = (gcnew System::Windows::Forms::Button());
		this->button14 = (gcnew System::Windows::Forms::Button());
		this->button15 = (gcnew System::Windows::Forms::Button());
		this->button16 = (gcnew System::Windows::Forms::Button());
		this->button17 = (gcnew System::Windows::Forms::Button());
		this->button18 = (gcnew System::Windows::Forms::Button());
		this->button19 = (gcnew System::Windows::Forms::Button());
		this->button20 = (gcnew System::Windows::Forms::Button());
		this->button21 = (gcnew System::Windows::Forms::Button());
		this->button22 = (gcnew System::Windows::Forms::Button());
		this->button23 = (gcnew System::Windows::Forms::Button());
		this->button24 = (gcnew System::Windows::Forms::Button());
		this->button25 = (gcnew System::Windows::Forms::Button());
		this->button26 = (gcnew System::Windows::Forms::Button());
		this->button27 = (gcnew System::Windows::Forms::Button());
		this->button28 = (gcnew System::Windows::Forms::Button());
		this->button29 = (gcnew System::Windows::Forms::Button());
		this->button30 = (gcnew System::Windows::Forms::Button());
		this->button31 = (gcnew System::Windows::Forms::Button());
		this->button32 = (gcnew System::Windows::Forms::Button());
		this->button33 = (gcnew System::Windows::Forms::Button());
		this->button34 = (gcnew System::Windows::Forms::Button());
		this->button35 = (gcnew System::Windows::Forms::Button());
		this->button36 = (gcnew System::Windows::Forms::Button());
		this->button37 = (gcnew System::Windows::Forms::Button());
		this->button38 = (gcnew System::Windows::Forms::Button());
		this->button39 = (gcnew System::Windows::Forms::Button());
		this->button40 = (gcnew System::Windows::Forms::Button());
		this->button41 = (gcnew System::Windows::Forms::Button());
		this->button42 = (gcnew System::Windows::Forms::Button());
		this->button43 = (gcnew System::Windows::Forms::Button());
		this->button44 = (gcnew System::Windows::Forms::Button());
		this->button45 = (gcnew System::Windows::Forms::Button());
		this->button46 = (gcnew System::Windows::Forms::Button());
		this->button47 = (gcnew System::Windows::Forms::Button());
		this->button48 = (gcnew System::Windows::Forms::Button());
		this->button49 = (gcnew System::Windows::Forms::Button());
		this->button50 = (gcnew System::Windows::Forms::Button());
		this->button51 = (gcnew System::Windows::Forms::Button());
		this->button52 = (gcnew System::Windows::Forms::Button());
		this->button53 = (gcnew System::Windows::Forms::Button());
		this->button54 = (gcnew System::Windows::Forms::Button());
		this->button55 = (gcnew System::Windows::Forms::Button());
		this->button56 = (gcnew System::Windows::Forms::Button());
		this->button57 = (gcnew System::Windows::Forms::Button());
		this->button58 = (gcnew System::Windows::Forms::Button());
		this->button59 = (gcnew System::Windows::Forms::Button());
		this->button60 = (gcnew System::Windows::Forms::Button());
		this->button61 = (gcnew System::Windows::Forms::Button());
		this->button62 = (gcnew System::Windows::Forms::Button());
		this->button63 = (gcnew System::Windows::Forms::Button());
		this->button64 = (gcnew System::Windows::Forms::Button());
		this->txtValue = (gcnew System::Windows::Forms::TextBox());
		this->lblEnter = (gcnew System::Windows::Forms::Label());
		this->lblInstructions = (gcnew System::Windows::Forms::Label());
		this->btnReset = (gcnew System::Windows::Forms::Button());
		this->btnEvaluate = (gcnew System::Windows::Forms::Button());
		this->timer1 = (gcnew System::Windows::Forms::Timer(this->components));
		this->lblPuzzleNo = (gcnew System::Windows::Forms::Label());
		this->btnPause = (gcnew System::Windows::Forms::Button());
		this->btnPlay = (gcnew System::Windows::Forms::Button());
		this->groupBox1 = (gcnew System::Windows::Forms::GroupBox());
		this->groupBox1->SuspendLayout();
		this->SuspendLayout();
		// 
		// button1
		// 
		this->button1->Location = System::Drawing::Point(57, 44);
		this->button1->Name = L"button1";
		this->button1->Size = System::Drawing::Size(33, 23);
		this->button1->TabIndex = 0;
		this->button1->UseVisualStyleBackColor = true;
		this->button1->Click += gcnew System::EventHandler(this, &MyForm::button_Click);
		// 
		// button2
		// 
		this->button2->Location = System::Drawing::Point(87, 44);
		this->button2->Name = L"button2";
		this->button2->Size = System::Drawing::Size(33, 23);
		this->button2->TabIndex = 1;
		this->button2->UseVisualStyleBackColor = true;
		this->button2->Click += gcnew System::EventHandler(this, &MyForm::button_Click);
		// 
		// button3
		// 
		this->button3->Location = System::Drawing::Point(59, 0);
		this->button3->Name = L"button3";
		this->button3->Size = System::Drawing::Size(33, 23);
		this->button3->TabIndex = 2;
		this->button3->UseVisualStyleBackColor = true;
		this->button3->Click += gcnew System::EventHandler(this, &MyForm::button_Click);
		// 
		// button4
		// 
		this->button4->Location = System::Drawing::Point(88, 0);
		this->button4->Name = L"button4";
		this->button4->Size = System::Drawing::Size(33, 23);
		this->button4->TabIndex = 3;
		this->button4->UseVisualStyleBackColor = true;
		this->button4->Click += gcnew System::EventHandler(this, &MyForm::button_Click);
		// 
		// button5
		// 
		this->button5->Location = System::Drawing::Point(117, 0);
		this->button5->Name = L"button5";
		this->button5->Size = System::Drawing::Size(33, 23);
		this->button5->TabIndex = 4;
		this->button5->UseVisualStyleBackColor = true;
		this->button5->Click += gcnew System::EventHandler(this, &MyForm::button_Click);
		// 
		// button6
		// 
		this->button6->Location = System::Drawing::Point(145, 0);
		this->button6->Name = L"button6";
		this->button6->Size = System::Drawing::Size(33, 23);
		this->button6->TabIndex = 5;
		this->button6->UseVisualStyleBackColor = true;
		this->button6->Click += gcnew System::EventHandler(this, &MyForm::button_Click);
		// 
		// button7
		// 
		this->button7->Location = System::Drawing::Point(174, 0);
		this->button7->Name = L"button7";
		this->button7->Size = System::Drawing::Size(33, 23);
		this->button7->TabIndex = 6;
		this->button7->UseVisualStyleBackColor = true;
		this->button7->Click += gcnew System::EventHandler(this, &MyForm::button_Click);
		// 
		// button8
		// 
		this->button8->Location = System::Drawing::Point(201, 0);
		this->button8->Name = L"button8";
		this->button8->Size = System::Drawing::Size(33, 23);
		this->button8->TabIndex = 7;
		this->button8->UseVisualStyleBackColor = true;
		this->button8->Click += gcnew System::EventHandler(this, &MyForm::button_Click);
		// 
		// button9
		// 
		this->button9->Location = System::Drawing::Point(0, 19);
		this->button9->Name = L"button9";
		this->button9->Size = System::Drawing::Size(33, 23);
		this->button9->TabIndex = 8;
		this->button9->UseVisualStyleBackColor = true;
		this->button9->Click += gcnew System::EventHandler(this, &MyForm::button_Click);
		// 
		// button10
		// 
		this->button10->Location = System::Drawing::Point(30, 19);
		this->button10->Name = L"button10";
		this->button10->Size = System::Drawing::Size(33, 23);
		this->button10->TabIndex = 9;
		this->button10->UseVisualStyleBackColor = true;
		this->button10->Click += gcnew System::EventHandler(this, &MyForm::button_Click);
		// 
		// button11
		// 
		this->button11->Location = System::Drawing::Point(59, 19);
		this->button11->Name = L"button11";
		this->button11->Size = System::Drawing::Size(33, 23);
		this->button11->TabIndex = 10;
		this->button11->UseVisualStyleBackColor = true;
		this->button11->Click += gcnew System::EventHandler(this, &MyForm::button_Click);
		// 
		// button12
		// 
		this->button12->Location = System::Drawing::Point(88, 19);
		this->button12->Name = L"button12";
		this->button12->Size = System::Drawing::Size(33, 23);
		this->button12->TabIndex = 11;
		this->button12->UseVisualStyleBackColor = true;
		this->button12->Click += gcnew System::EventHandler(this, &MyForm::button_Click);
		// 
		// button13
		// 
		this->button13->Location = System::Drawing::Point(117, 19);
		this->button13->Name = L"button13";
		this->button13->Size = System::Drawing::Size(33, 23);
		this->button13->TabIndex = 12;
		this->button13->UseVisualStyleBackColor = true;
		this->button13->Click += gcnew System::EventHandler(this, &MyForm::button_Click);
		// 
		// button14
		// 
		this->button14->Location = System::Drawing::Point(145, 19);
		this->button14->Name = L"button14";
		this->button14->Size = System::Drawing::Size(33, 23);
		this->button14->TabIndex = 13;
		this->button14->UseVisualStyleBackColor = true;
		this->button14->Click += gcnew System::EventHandler(this, &MyForm::button_Click);
		// 
		// button15
		// 
		this->button15->Location = System::Drawing::Point(174, 19);
		this->button15->Name = L"button15";
		this->button15->Size = System::Drawing::Size(33, 23);
		this->button15->TabIndex = 14;
		this->button15->UseVisualStyleBackColor = true;
		this->button15->Click += gcnew System::EventHandler(this, &MyForm::button_Click);
		// 
		// button16
		// 
		this->button16->Location = System::Drawing::Point(201, 19);
		this->button16->Name = L"button16";
		this->button16->Size = System::Drawing::Size(33, 23);
		this->button16->TabIndex = 15;
		this->button16->UseVisualStyleBackColor = true;
		this->button16->Click += gcnew System::EventHandler(this, &MyForm::button_Click);
		// 
		// button17
		// 
		this->button17->Location = System::Drawing::Point(0, 38);
		this->button17->Name = L"button17";
		this->button17->Size = System::Drawing::Size(33, 23);
		this->button17->TabIndex = 16;
		this->button17->UseVisualStyleBackColor = true;
		this->button17->Click += gcnew System::EventHandler(this, &MyForm::button_Click);
		// 
		// button18
		// 
		this->button18->Location = System::Drawing::Point(30, 38);
		this->button18->Name = L"button18";
		this->button18->Size = System::Drawing::Size(33, 23);
		this->button18->TabIndex = 17;
		this->button18->UseVisualStyleBackColor = true;
		this->button18->Click += gcnew System::EventHandler(this, &MyForm::button_Click);
		// 
		// button19
		// 
		this->button19->Location = System::Drawing::Point(59, 38);
		this->button19->Name = L"button19";
		this->button19->Size = System::Drawing::Size(33, 23);
		this->button19->TabIndex = 18;
		this->button19->UseVisualStyleBackColor = true;
		this->button19->Click += gcnew System::EventHandler(this, &MyForm::button_Click);
		// 
		// button20
		// 
		this->button20->Location = System::Drawing::Point(88, 38);
		this->button20->Name = L"button20";
		this->button20->Size = System::Drawing::Size(33, 23);
		this->button20->TabIndex = 19;
		this->button20->UseVisualStyleBackColor = true;
		this->button20->Click += gcnew System::EventHandler(this, &MyForm::button_Click);
		// 
		// button21
		// 
		this->button21->Location = System::Drawing::Point(117, 38);
		this->button21->Name = L"button21";
		this->button21->Size = System::Drawing::Size(33, 23);
		this->button21->TabIndex = 20;
		this->button21->UseVisualStyleBackColor = true;
		this->button21->Click += gcnew System::EventHandler(this, &MyForm::button_Click);
		// 
		// button22
		// 
		this->button22->Location = System::Drawing::Point(145, 38);
		this->button22->Name = L"button22";
		this->button22->Size = System::Drawing::Size(33, 23);
		this->button22->TabIndex = 21;
		this->button22->UseVisualStyleBackColor = true;
		this->button22->Click += gcnew System::EventHandler(this, &MyForm::button_Click);
		// 
		// button23
		// 
		this->button23->Location = System::Drawing::Point(174, 38);
		this->button23->Name = L"button23";
		this->button23->Size = System::Drawing::Size(33, 23);
		this->button23->TabIndex = 22;
		this->button23->UseVisualStyleBackColor = true;
		this->button23->Click += gcnew System::EventHandler(this, &MyForm::button_Click);
		// 
		// button24
		// 
		this->button24->Location = System::Drawing::Point(201, 38);
		this->button24->Name = L"button24";
		this->button24->Size = System::Drawing::Size(33, 23);
		this->button24->TabIndex = 23;
		this->button24->UseVisualStyleBackColor = true;
		this->button24->Click += gcnew System::EventHandler(this, &MyForm::button_Click);
		// 
		// button25
		// 
		this->button25->Location = System::Drawing::Point(0, 58);
		this->button25->Name = L"button25";
		this->button25->Size = System::Drawing::Size(33, 23);
		this->button25->TabIndex = 24;
		this->button25->UseVisualStyleBackColor = true;
		this->button25->Click += gcnew System::EventHandler(this, &MyForm::button_Click);
		// 
		// button26
		// 
		this->button26->Location = System::Drawing::Point(30, 58);
		this->button26->Name = L"button26";
		this->button26->Size = System::Drawing::Size(33, 23);
		this->button26->TabIndex = 25;
		this->button26->UseVisualStyleBackColor = true;
		this->button26->Click += gcnew System::EventHandler(this, &MyForm::button_Click);
		// 
		// button27
		// 
		this->button27->Location = System::Drawing::Point(59, 58);
		this->button27->Name = L"button27";
		this->button27->Size = System::Drawing::Size(33, 23);
		this->button27->TabIndex = 26;
		this->button27->UseVisualStyleBackColor = true;
		this->button27->Click += gcnew System::EventHandler(this, &MyForm::button_Click);
		// 
		// button28
		// 
		this->button28->Location = System::Drawing::Point(88, 58);
		this->button28->Name = L"button28";
		this->button28->Size = System::Drawing::Size(33, 23);
		this->button28->TabIndex = 27;
		this->button28->UseVisualStyleBackColor = true;
		this->button28->Click += gcnew System::EventHandler(this, &MyForm::button_Click);
		// 
		// button29
		// 
		this->button29->Location = System::Drawing::Point(117, 58);
		this->button29->Name = L"button29";
		this->button29->Size = System::Drawing::Size(33, 23);
		this->button29->TabIndex = 28;
		this->button29->UseVisualStyleBackColor = true;
		this->button29->Click += gcnew System::EventHandler(this, &MyForm::button_Click);
		// 
		// button30
		// 
		this->button30->Location = System::Drawing::Point(145, 58);
		this->button30->Name = L"button30";
		this->button30->Size = System::Drawing::Size(33, 23);
		this->button30->TabIndex = 29;
		this->button30->UseVisualStyleBackColor = true;
		this->button30->Click += gcnew System::EventHandler(this, &MyForm::button_Click);
		// 
		// button31
		// 
		this->button31->Location = System::Drawing::Point(174, 58);
		this->button31->Name = L"button31";
		this->button31->Size = System::Drawing::Size(33, 23);
		this->button31->TabIndex = 30;
		this->button31->UseVisualStyleBackColor = true;
		this->button31->Click += gcnew System::EventHandler(this, &MyForm::button_Click);
		// 
		// button32
		// 
		this->button32->Location = System::Drawing::Point(201, 58);
		this->button32->Name = L"button32";
		this->button32->Size = System::Drawing::Size(33, 23);
		this->button32->TabIndex = 31;
		this->button32->UseVisualStyleBackColor = true;
		this->button32->Click += gcnew System::EventHandler(this, &MyForm::button_Click);
		// 
		// button33
		// 
		this->button33->Location = System::Drawing::Point(0, 78);
		this->button33->Name = L"button33";
		this->button33->Size = System::Drawing::Size(33, 23);
		this->button33->TabIndex = 32;
		this->button33->UseVisualStyleBackColor = true;
		this->button33->Click += gcnew System::EventHandler(this, &MyForm::button_Click);
		// 
		// button34
		// 
		this->button34->Location = System::Drawing::Point(30, 78);
		this->button34->Name = L"button34";
		this->button34->Size = System::Drawing::Size(33, 23);
		this->button34->TabIndex = 33;
		this->button34->UseVisualStyleBackColor = true;
		this->button34->Click += gcnew System::EventHandler(this, &MyForm::button_Click);
		// 
		// button35
		// 
		this->button35->Location = System::Drawing::Point(59, 78);
		this->button35->Name = L"button35";
		this->button35->Size = System::Drawing::Size(33, 23);
		this->button35->TabIndex = 34;
		this->button35->UseVisualStyleBackColor = true;
		this->button35->Click += gcnew System::EventHandler(this, &MyForm::button_Click);
		// 
		// button36
		// 
		this->button36->Location = System::Drawing::Point(88, 78);
		this->button36->Name = L"button36";
		this->button36->Size = System::Drawing::Size(33, 23);
		this->button36->TabIndex = 35;
		this->button36->UseVisualStyleBackColor = true;
		this->button36->Click += gcnew System::EventHandler(this, &MyForm::button_Click);
		// 
		// button37
		// 
		this->button37->Location = System::Drawing::Point(117, 78);
		this->button37->Name = L"button37";
		this->button37->Size = System::Drawing::Size(33, 23);
		this->button37->TabIndex = 36;
		this->button37->UseVisualStyleBackColor = true;
		this->button37->Click += gcnew System::EventHandler(this, &MyForm::button_Click);
		// 
		// button38
		// 
		this->button38->Location = System::Drawing::Point(145, 78);
		this->button38->Name = L"button38";
		this->button38->Size = System::Drawing::Size(33, 23);
		this->button38->TabIndex = 37;
		this->button38->UseVisualStyleBackColor = true;
		this->button38->Click += gcnew System::EventHandler(this, &MyForm::button_Click);
		// 
		// button39
		// 
		this->button39->Location = System::Drawing::Point(174, 78);
		this->button39->Name = L"button39";
		this->button39->Size = System::Drawing::Size(33, 23);
		this->button39->TabIndex = 38;
		this->button39->UseVisualStyleBackColor = true;
		this->button39->Click += gcnew System::EventHandler(this, &MyForm::button_Click);
		// 
		// button40
		// 
		this->button40->Location = System::Drawing::Point(201, 78);
		this->button40->Name = L"button40";
		this->button40->Size = System::Drawing::Size(33, 23);
		this->button40->TabIndex = 39;
		this->button40->UseVisualStyleBackColor = true;
		this->button40->Click += gcnew System::EventHandler(this, &MyForm::button_Click);
		// 
		// button41
		// 
		this->button41->Location = System::Drawing::Point(0, 97);
		this->button41->Name = L"button41";
		this->button41->Size = System::Drawing::Size(33, 23);
		this->button41->TabIndex = 40;
		this->button41->UseVisualStyleBackColor = true;
		this->button41->Click += gcnew System::EventHandler(this, &MyForm::button_Click);
		// 
		// button42
		// 
		this->button42->Location = System::Drawing::Point(30, 97);
		this->button42->Name = L"button42";
		this->button42->Size = System::Drawing::Size(33, 23);
		this->button42->TabIndex = 41;
		this->button42->UseVisualStyleBackColor = true;
		this->button42->Click += gcnew System::EventHandler(this, &MyForm::button_Click);
		// 
		// button43
		// 
		this->button43->Location = System::Drawing::Point(59, 97);
		this->button43->Name = L"button43";
		this->button43->Size = System::Drawing::Size(33, 23);
		this->button43->TabIndex = 42;
		this->button43->UseVisualStyleBackColor = true;
		this->button43->Click += gcnew System::EventHandler(this, &MyForm::button_Click);
		// 
		// button44
		// 
		this->button44->Location = System::Drawing::Point(88, 97);
		this->button44->Name = L"button44";
		this->button44->Size = System::Drawing::Size(33, 23);
		this->button44->TabIndex = 43;
		this->button44->UseVisualStyleBackColor = true;
		this->button44->Click += gcnew System::EventHandler(this, &MyForm::button_Click);
		// 
		// button45
		// 
		this->button45->Location = System::Drawing::Point(117, 97);
		this->button45->Name = L"button45";
		this->button45->Size = System::Drawing::Size(33, 23);
		this->button45->TabIndex = 44;
		this->button45->UseVisualStyleBackColor = true;
		this->button45->Click += gcnew System::EventHandler(this, &MyForm::button_Click);
		// 
		// button46
		// 
		this->button46->Location = System::Drawing::Point(145, 97);
		this->button46->Name = L"button46";
		this->button46->Size = System::Drawing::Size(33, 23);
		this->button46->TabIndex = 45;
		this->button46->UseVisualStyleBackColor = true;
		this->button46->Click += gcnew System::EventHandler(this, &MyForm::button_Click);
		// 
		// button47
		// 
		this->button47->Location = System::Drawing::Point(174, 97);
		this->button47->Name = L"button47";
		this->button47->Size = System::Drawing::Size(33, 23);
		this->button47->TabIndex = 46;
		this->button47->UseVisualStyleBackColor = true;
		this->button47->Click += gcnew System::EventHandler(this, &MyForm::button_Click);
		// 
		// button48
		// 
		this->button48->Location = System::Drawing::Point(201, 97);
		this->button48->Name = L"button48";
		this->button48->Size = System::Drawing::Size(33, 23);
		this->button48->TabIndex = 47;
		this->button48->UseVisualStyleBackColor = true;
		this->button48->Click += gcnew System::EventHandler(this, &MyForm::button_Click);
		// 
		// button49
		// 
		this->button49->Location = System::Drawing::Point(0, 116);
		this->button49->Name = L"button49";
		this->button49->Size = System::Drawing::Size(33, 23);
		this->button49->TabIndex = 48;
		this->button49->UseVisualStyleBackColor = true;
		this->button49->Click += gcnew System::EventHandler(this, &MyForm::button_Click);
		// 
		// button50
		// 
		this->button50->Location = System::Drawing::Point(30, 116);
		this->button50->Name = L"button50";
		this->button50->Size = System::Drawing::Size(33, 23);
		this->button50->TabIndex = 49;
		this->button50->UseVisualStyleBackColor = true;
		this->button50->Click += gcnew System::EventHandler(this, &MyForm::button_Click);
		// 
		// button51
		// 
		this->button51->Location = System::Drawing::Point(59, 116);
		this->button51->Name = L"button51";
		this->button51->Size = System::Drawing::Size(33, 23);
		this->button51->TabIndex = 50;
		this->button51->UseVisualStyleBackColor = true;
		this->button51->Click += gcnew System::EventHandler(this, &MyForm::button_Click);
		// 
		// button52
		// 
		this->button52->Location = System::Drawing::Point(88, 116);
		this->button52->Name = L"button52";
		this->button52->Size = System::Drawing::Size(33, 23);
		this->button52->TabIndex = 51;
		this->button52->UseVisualStyleBackColor = true;
		this->button52->Click += gcnew System::EventHandler(this, &MyForm::button_Click);
		// 
		// button53
		// 
		this->button53->Location = System::Drawing::Point(117, 116);
		this->button53->Name = L"button53";
		this->button53->Size = System::Drawing::Size(33, 23);
		this->button53->TabIndex = 52;
		this->button53->UseVisualStyleBackColor = true;
		this->button53->Click += gcnew System::EventHandler(this, &MyForm::button_Click);
		// 
		// button54
		// 
		this->button54->Location = System::Drawing::Point(145, 116);
		this->button54->Name = L"button54";
		this->button54->Size = System::Drawing::Size(33, 23);
		this->button54->TabIndex = 53;
		this->button54->UseVisualStyleBackColor = true;
		this->button54->Click += gcnew System::EventHandler(this, &MyForm::button_Click);
		// 
		// button55
		// 
		this->button55->Location = System::Drawing::Point(174, 116);
		this->button55->Name = L"button55";
		this->button55->Size = System::Drawing::Size(33, 23);
		this->button55->TabIndex = 54;
		this->button55->UseVisualStyleBackColor = true;
		this->button55->Click += gcnew System::EventHandler(this, &MyForm::button_Click);
		// 
		// button56
		// 
		this->button56->Location = System::Drawing::Point(201, 116);
		this->button56->Name = L"button56";
		this->button56->Size = System::Drawing::Size(33, 23);
		this->button56->TabIndex = 55;
		this->button56->UseVisualStyleBackColor = true;
		this->button56->Click += gcnew System::EventHandler(this, &MyForm::button_Click);
		// 
		// button57
		// 
		this->button57->Location = System::Drawing::Point(0, 135);
		this->button57->Name = L"button57";
		this->button57->Size = System::Drawing::Size(33, 23);
		this->button57->TabIndex = 56;
		this->button57->UseVisualStyleBackColor = true;
		this->button57->Click += gcnew System::EventHandler(this, &MyForm::button_Click);
		// 
		// button58
		// 
		this->button58->Location = System::Drawing::Point(30, 135);
		this->button58->Name = L"button58";
		this->button58->Size = System::Drawing::Size(33, 23);
		this->button58->TabIndex = 57;
		this->button58->UseVisualStyleBackColor = true;
		this->button58->Click += gcnew System::EventHandler(this, &MyForm::button_Click);
		// 
		// button59
		// 
		this->button59->Location = System::Drawing::Point(59, 135);
		this->button59->Name = L"button59";
		this->button59->Size = System::Drawing::Size(33, 23);
		this->button59->TabIndex = 58;
		this->button59->UseVisualStyleBackColor = true;
		this->button59->Click += gcnew System::EventHandler(this, &MyForm::button_Click);
		// 
		// button60
		// 
		this->button60->Location = System::Drawing::Point(88, 135);
		this->button60->Name = L"button60";
		this->button60->Size = System::Drawing::Size(33, 23);
		this->button60->TabIndex = 59;
		this->button60->UseVisualStyleBackColor = true;
		this->button60->Click += gcnew System::EventHandler(this, &MyForm::button_Click);
		// 
		// button61
		// 
		this->button61->Location = System::Drawing::Point(117, 135);
		this->button61->Name = L"button61";
		this->button61->Size = System::Drawing::Size(33, 23);
		this->button61->TabIndex = 60;
		this->button61->UseVisualStyleBackColor = true;
		this->button61->Click += gcnew System::EventHandler(this, &MyForm::button_Click);
		// 
		// button62
		// 
		this->button62->Location = System::Drawing::Point(145, 135);
		this->button62->Name = L"button62";
		this->button62->Size = System::Drawing::Size(33, 23);
		this->button62->TabIndex = 61;
		this->button62->UseVisualStyleBackColor = true;
		this->button62->Click += gcnew System::EventHandler(this, &MyForm::button_Click);
		// 
		// button63
		// 
		this->button63->Location = System::Drawing::Point(174, 135);
		this->button63->Name = L"button63";
		this->button63->Size = System::Drawing::Size(33, 23);
		this->button63->TabIndex = 62;
		this->button63->UseVisualStyleBackColor = true;
		this->button63->Click += gcnew System::EventHandler(this, &MyForm::button_Click);
		// 
		// button64
		// 
		this->button64->Location = System::Drawing::Point(201, 135);
		this->button64->Name = L"button64";
		this->button64->Size = System::Drawing::Size(33, 23);
		this->button64->TabIndex = 63;
		this->button64->UseVisualStyleBackColor = true;
		this->button64->Click += gcnew System::EventHandler(this, &MyForm::button_Click);
		// 
		// txtValue
		// 
		this->txtValue->AcceptsReturn = true;
		this->txtValue->Location = System::Drawing::Point(164, 215);
		this->txtValue->Name = L"txtValue";
		this->txtValue->Size = System::Drawing::Size(100, 20);
		this->txtValue->TabIndex = 64;
		// 
		// lblEnter
		// 
		this->lblEnter->AutoSize = true;
		this->lblEnter->Location = System::Drawing::Point(84, 222);
		this->lblEnter->Name = L"lblEnter";
		this->lblEnter->Size = System::Drawing::Size(73, 13);
		this->lblEnter->TabIndex = 65;
		this->lblEnter->Text = L"Enter a value:";
		// 
		// lblInstructions
		// 
		this->lblInstructions->AutoSize = true;
		this->lblInstructions->Location = System::Drawing::Point(54, 251);
		this->lblInstructions->Name = L"lblInstructions";
		this->lblInstructions->Size = System::Drawing::Size(198, 65);
		this->lblInstructions->TabIndex = 66;
		this->lblInstructions->Text = L"How to play:\r\n\r\n- Enter a value from 1-64 into the textbox\r\n- Select a box from t"
			L"he grid you \r\n   would like to enter the value into";
		// 
		// btnReset
		// 
		this->btnReset->Location = System::Drawing::Point(177, 350);
		this->btnReset->Name = L"btnReset";
		this->btnReset->Size = System::Drawing::Size(75, 23);
		this->btnReset->TabIndex = 67;
		this->btnReset->Text = L"Reset";
		this->btnReset->UseVisualStyleBackColor = true;
		this->btnReset->Click += gcnew System::EventHandler(this, &MyForm::btnReset_Click);
		// 
		// btnEvaluate
		// 
		this->btnEvaluate->Location = System::Drawing::Point(258, 350);
		this->btnEvaluate->Name = L"btnEvaluate";
		this->btnEvaluate->Size = System::Drawing::Size(75, 23);
		this->btnEvaluate->TabIndex = 68;
		this->btnEvaluate->Text = L"Evaulate";
		this->btnEvaluate->UseVisualStyleBackColor = true;
		this->btnEvaluate->Click += gcnew System::EventHandler(this, &MyForm::btnEvaluate_Click);
		// 
		// timer1
		// 
		this->timer1->Enabled = true;
		// 
		// lblPuzzleNo
		// 
		this->lblPuzzleNo->AutoSize = true;
		this->lblPuzzleNo->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 8.25F, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point,
			static_cast<System::Byte>(0)));
		this->lblPuzzleNo->Location = System::Drawing::Point(12, 18);
		this->lblPuzzleNo->Name = L"lblPuzzleNo";
		this->lblPuzzleNo->Size = System::Drawing::Size(56, 13);
		this->lblPuzzleNo->TabIndex = 69;
		this->lblPuzzleNo->Text = L"Puzzle A";
		// 
		// btnPause
		// 
		this->btnPause->Location = System::Drawing::Point(96, 350);
		this->btnPause->Name = L"btnPause";
		this->btnPause->Size = System::Drawing::Size(75, 23);
		this->btnPause->TabIndex = 70;
		this->btnPause->Text = L"Pause";
		this->btnPause->UseVisualStyleBackColor = true;
		// 
		// btnPlay
		// 
		this->btnPlay->Location = System::Drawing::Point(15, 350);
		this->btnPlay->Name = L"btnPlay";
		this->btnPlay->Size = System::Drawing::Size(75, 23);
		this->btnPlay->TabIndex = 71;
		this->btnPlay->Text = L"Play";
		this->btnPlay->UseVisualStyleBackColor = true;
		this->btnPlay->Click += gcnew System::EventHandler(this, &MyForm::btnPlay_Click);
		// 
		// groupBox1
		// 
		this->groupBox1->Controls->Add(this->button4);
		this->groupBox1->Controls->Add(this->button5);
		this->groupBox1->Controls->Add(this->button3);
		this->groupBox1->Controls->Add(this->button6);
		this->groupBox1->Controls->Add(this->button7);
		this->groupBox1->Controls->Add(this->button8);
		this->groupBox1->Controls->Add(this->button9);
		this->groupBox1->Controls->Add(this->button16);
		this->groupBox1->Controls->Add(this->button61);
		this->groupBox1->Controls->Add(this->button62);
		this->groupBox1->Controls->Add(this->button60);
		this->groupBox1->Controls->Add(this->button63);
		this->groupBox1->Controls->Add(this->button64);
		this->groupBox1->Controls->Add(this->button10);
		this->groupBox1->Controls->Add(this->button59);
		this->groupBox1->Controls->Add(this->button15);
		this->groupBox1->Controls->Add(this->button58);
		this->groupBox1->Controls->Add(this->button14);
		this->groupBox1->Controls->Add(this->button11);
		this->groupBox1->Controls->Add(this->button12);
		this->groupBox1->Controls->Add(this->button57);
		this->groupBox1->Controls->Add(this->button13);
		this->groupBox1->Controls->Add(this->button24);
		this->groupBox1->Controls->Add(this->button17);
		this->groupBox1->Controls->Add(this->button52);
		this->groupBox1->Controls->Add(this->button53);
		this->groupBox1->Controls->Add(this->button54);
		this->groupBox1->Controls->Add(this->button55);
		this->groupBox1->Controls->Add(this->button56);
		this->groupBox1->Controls->Add(this->button18);
		this->groupBox1->Controls->Add(this->button51);
		this->groupBox1->Controls->Add(this->button23);
		this->groupBox1->Controls->Add(this->button50);
		this->groupBox1->Controls->Add(this->button19);
		this->groupBox1->Controls->Add(this->button22);
		this->groupBox1->Controls->Add(this->button20);
		this->groupBox1->Controls->Add(this->button49);
		this->groupBox1->Controls->Add(this->button21);
		this->groupBox1->Controls->Add(this->button25);
		this->groupBox1->Controls->Add(this->button32);
		this->groupBox1->Controls->Add(this->button45);
		this->groupBox1->Controls->Add(this->button46);
		this->groupBox1->Controls->Add(this->button44);
		this->groupBox1->Controls->Add(this->button47);
		this->groupBox1->Controls->Add(this->button43);
		this->groupBox1->Controls->Add(this->button48);
		this->groupBox1->Controls->Add(this->button26);
		this->groupBox1->Controls->Add(this->button31);
		this->groupBox1->Controls->Add(this->button30);
		this->groupBox1->Controls->Add(this->button27);
		this->groupBox1->Controls->Add(this->button42);
		this->groupBox1->Controls->Add(this->button28);
		this->groupBox1->Controls->Add(this->button41);
		this->groupBox1->Controls->Add(this->button29);
		this->groupBox1->Controls->Add(this->button40);
		this->groupBox1->Controls->Add(this->button33);
		this->groupBox1->Controls->Add(this->button37);
		this->groupBox1->Controls->Add(this->button38);
		this->groupBox1->Controls->Add(this->button36);
		this->groupBox1->Controls->Add(this->button39);
		this->groupBox1->Controls->Add(this->button35);
		this->groupBox1->Controls->Add(this->button34);
		this->groupBox1->Location = System::Drawing::Point(57, 44);
		this->groupBox1->Name = L"groupBox1";
		this->groupBox1->Size = System::Drawing::Size(234, 158);
		this->groupBox1->TabIndex = 72;
		this->groupBox1->TabStop = false;
		this->groupBox1->Text = L"groupBox1";
		// 
		// MyForm
		// 
		this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
		this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
		this->ClientSize = System::Drawing::Size(352, 402);
		this->Controls->Add(this->btnPlay);
		this->Controls->Add(this->btnPause);
		this->Controls->Add(this->lblPuzzleNo);
		this->Controls->Add(this->btnEvaluate);
		this->Controls->Add(this->btnReset);
		this->Controls->Add(this->lblInstructions);
		this->Controls->Add(this->lblEnter);
		this->Controls->Add(this->txtValue);
		this->Controls->Add(this->button2);
		this->Controls->Add(this->button1);
		this->Controls->Add(this->groupBox1);
		this->Name = L"MyForm";
		this->Text = L"Connect 64 by Chuma Okafor & James Irwin";
		this->groupBox1->ResumeLayout(false);
		this->ResumeLayout(false);
		this->PerformLayout();
	}

#pragma endregion

}