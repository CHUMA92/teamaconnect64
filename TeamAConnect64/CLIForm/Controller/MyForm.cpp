#include "../TeamAConnect64.h"
#include "../Puzzle.h"

namespace CLIForm {

	MyForm::MyForm(void)
	{
		InitializeComponent();
		
		this->controlButton(false);
		this->txtValue->Focus();
		this->resourceManager = gcnew ResourceManager("CLIForm.OutputStrings", this->GetType()->Assembly);
	}

	/// <summary>
	/// Clean up any resources being used.
	/// </summary>
	MyForm::~MyForm()
	{
		if (components)
		{
			delete components;
		}
	}
}
