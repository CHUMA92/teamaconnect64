
#include "TeamAConnect64.h" //the header name for your form

using namespace CLIForm; //the project name in your header file

[STAThreadAttribute]
int main(array<System::String ^> ^args)
{
	Application::EnableVisualStyles();
	Application::SetCompatibleTextRenderingDefault(false);

	Application::Run(gcnew MyForm());
	return 0;
}
