#pragma once

namespace CLIForm {

	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;
	using namespace System::Resources;
	using namespace System;
	using namespace System::IO;


	/// <summary>
	/// Summary for MyForm
	/// </summary>
	public ref class MyForm : public System::Windows::Forms::Form
	{
	public:
		MyForm(void);

	protected:
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		~MyForm();

	private: ResourceManager^ resourceManager;
	private: System::Windows::Forms::Button^  button1;
	private: System::Windows::Forms::Button^  button2;
	private: System::Windows::Forms::Button^  button3;
	private: System::Windows::Forms::Button^  button4;
	private: System::Windows::Forms::Button^  button5;
	private: System::Windows::Forms::Button^  button6;
	private: System::Windows::Forms::Button^  button7;
	private: System::Windows::Forms::Button^  button8;
	private: System::Windows::Forms::Button^  button9;
	private: System::Windows::Forms::Button^  button10;
	private: System::Windows::Forms::Button^  button11;
	private: System::Windows::Forms::Button^  button12;
	private: System::Windows::Forms::Button^  button13;
	private: System::Windows::Forms::Button^  button14;
	private: System::Windows::Forms::Button^  button15;
	private: System::Windows::Forms::Button^  button16;
	private: System::Windows::Forms::Button^  button17;
	private: System::Windows::Forms::Button^  button18;
	private: System::Windows::Forms::Button^  button19;
	private: System::Windows::Forms::Button^  button20;
	private: System::Windows::Forms::Button^  button21;
	private: System::Windows::Forms::Button^  button22;
	private: System::Windows::Forms::Button^  button23;
	private: System::Windows::Forms::Button^  button24;
	private: System::Windows::Forms::Button^  button25;
	private: System::Windows::Forms::Button^  button26;
	private: System::Windows::Forms::Button^  button27;
	private: System::Windows::Forms::Button^  button28;
	private: System::Windows::Forms::Button^  button29;
	private: System::Windows::Forms::Button^  button30;
	private: System::Windows::Forms::Button^  button31;
	private: System::Windows::Forms::Button^  button32;
	private: System::Windows::Forms::Button^  button33;
	private: System::Windows::Forms::Button^  button34;
	private: System::Windows::Forms::Button^  button35;
	private: System::Windows::Forms::Button^  button36;
	private: System::Windows::Forms::Button^  button37;
	private: System::Windows::Forms::Button^  button38;
	private: System::Windows::Forms::Button^  button39;
	private: System::Windows::Forms::Button^  button40;
	private: System::Windows::Forms::Button^  button41;
	private: System::Windows::Forms::Button^  button42;
	private: System::Windows::Forms::Button^  button43;
	private: System::Windows::Forms::Button^  button44;
	private: System::Windows::Forms::Button^  button45;
	private: System::Windows::Forms::Button^  button46;
	private: System::Windows::Forms::Button^  button47;
	private: System::Windows::Forms::Button^  button48;
	private: System::Windows::Forms::Button^  button49;
	private: System::Windows::Forms::Button^  button50;
	private: System::Windows::Forms::Button^  button51;
	private: System::Windows::Forms::Button^  button52;
	private: System::Windows::Forms::Button^  button53;
	private: System::Windows::Forms::Button^  button54;
	private: System::Windows::Forms::Button^  button55;
	private: System::Windows::Forms::Button^  button56;
	private: System::Windows::Forms::Button^  button57;
	private: System::Windows::Forms::Button^  button58;
	private: System::Windows::Forms::Button^  button59;
	private: System::Windows::Forms::Button^  button60;
	private: System::Windows::Forms::Button^  button61;
	private: System::Windows::Forms::Button^  button62;
	private: System::Windows::Forms::Button^  button63;
	private: System::Windows::Forms::Button^  button64;
	private: System::Windows::Forms::TextBox^  txtValue;
	private: System::Windows::Forms::Label^  lblEnter;
	private: System::Windows::Forms::Label^  lblInstructions;
	private: System::Windows::Forms::Button^  btnReset;
	private: System::Windows::Forms::Button^  btnEvaluate;
	private: System::Windows::Forms::Timer^  timer1;
	private: System::Windows::Forms::Label^  lblPuzzleNo;
	private: System::Windows::Forms::Button^  btnPause;
	private: System::Windows::Forms::Button^  btnPlay;
	private: System::Windows::Forms::GroupBox^  groupBox1;
	private: array<System::Windows::Forms::Button^>^ buttonCollection;
	private: array<String^>^ fakePuzzle;
	


	private: System::ComponentModel::IContainer^  components;

	private:
		/// <summary>
		/// Required designer variable.
		/// </summary>


#pragma region Windows Form Designer generated codek
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		void InitializeComponent(void);


#pragma endregion

	private:
		void resetTextBox() {
			this->txtValue->Clear();
			this->txtValue->Focus();
		}
		bool checkToSeeIfNumberIsAlreadyThere(String^ stringToCheck){
			bool letterAlreadyInPuzzle = false;
			for each (Button ^button in this->buttonCollection) {
				if (button->Text == stringToCheck || button->Text == "") {
					letterAlreadyInPuzzle = true;
				}
			}
			return letterAlreadyInPuzzle;
		}
		bool checkLetters(){
				int number;

				if (this->txtValue->Text == ""){
					return true;
				}
				else if (!Int32::TryParse(this->txtValue->Text, number)) {
					MessageBox::Show(L"First box wasn't an integer");
					resetTextBox();
					return false;
				}
				else if (number < 1 || number > 64) {
					MessageBox::Show(L"Integer not in range");
					resetTextBox();
					return false;
				}
				else if (checkToSeeIfNumberIsAlreadyThere(this->txtValue->Text)) {
					MessageBox::Show(L"Number already in puzzle");
					resetTextBox();
					return false;
				}
				else {
					return true;
				}
			}

		void setUpButtonArray(){
			if (this->buttonCollection != nullptr) {
				this->buttonCollection[63] = (this->button64);
				this->buttonCollection[62] = (this->button63);
				this->buttonCollection[61] = (this->button62);
				this->buttonCollection[60] = (this->button61);
				this->buttonCollection[59] = (this->button60);
				this->buttonCollection[58] = (this->button59);
				this->buttonCollection[57] = (this->button58);
				this->buttonCollection[56] = (this->button57);
				this->buttonCollection[55] = (this->button56);
				this->buttonCollection[54] = (this->button55);
				this->buttonCollection[53] = (this->button54);
				this->buttonCollection[52] = (this->button53);
				this->buttonCollection[51] = (this->button52);
				this->buttonCollection[50] = (this->button51);
				this->buttonCollection[49] = (this->button50);
				this->buttonCollection[48] = (this->button49);
				this->buttonCollection[47] = (this->button48);
				this->buttonCollection[46] = (this->button47);
				this->buttonCollection[45] = (this->button46);
				this->buttonCollection[44] = (this->button45);
				this->buttonCollection[43] = (this->button44);
				this->buttonCollection[42] = (this->button43);
				this->buttonCollection[41] = (this->button42);
				this->buttonCollection[40] = (this->button41);
				this->buttonCollection[39] = (this->button40);
				this->buttonCollection[38] = (this->button39);
				this->buttonCollection[37] = (this->button38);
				this->buttonCollection[36] = (this->button37);
				this->buttonCollection[35] = (this->button36);
				this->buttonCollection[34] = (this->button35);
				this->buttonCollection[33] = (this->button34);
				this->buttonCollection[32] = (this->button33);
				this->buttonCollection[31] = (this->button32);
				this->buttonCollection[30] = (this->button31);
				this->buttonCollection[29] = (this->button30);
				this->buttonCollection[28] = (this->button29);
				this->buttonCollection[27] = (this->button28);
				this->buttonCollection[26] = (this->button27);
				this->buttonCollection[25] = (this->button26);
				this->buttonCollection[24] = (this->button25);
				this->buttonCollection[23] = (this->button24);
				this->buttonCollection[22] = (this->button23);
				this->buttonCollection[21] = (this->button22);
				this->buttonCollection[20] = (this->button21);
				this->buttonCollection[19] = (this->button20);
				this->buttonCollection[18] = (this->button19);
				this->buttonCollection[17] = (this->button18);
				this->buttonCollection[16] = (this->button17);
				this->buttonCollection[15] = (this->button16);
				this->buttonCollection[14] = (this->button15);
				this->buttonCollection[13] = (this->button14);
				this->buttonCollection[12] = (this->button13);
				this->buttonCollection[11] = (this->button12);
				this->buttonCollection[10] = (this->button11);
				this->buttonCollection[9] = (this->button10);
				this->buttonCollection[8] = (this->button9);
				this->buttonCollection[7] = (this->button8);
				this->buttonCollection[6] = (this->button7);
				this->buttonCollection[5] = (this->button6);
				this->buttonCollection[4] = (this->button5);
				this->buttonCollection[3] = (this->button4);
				this->buttonCollection[2] = (this->button3);
				this->buttonCollection[1] = (this->button2);
				this->buttonCollection[0] = (this->button1);
			}
				
			}

		void setButtonsToPuzzle(array<String^>^ puzzle){
				for (int i = 0; this->buttonCollection->Length > i; i++){
					if (puzzle[i] != ""){
						this->buttonCollection[i]->Text = puzzle[i];
						this->buttonCollection[i]->Enabled = false;
					}
				}
			}
		bool isPuzzleFilled(array<System::Windows::Forms::Button^>^ buttonArray){
			bool isFilled = true;

			for (int i = 0; buttonArray->Length - 1 >= i; i++){
				if (buttonArray[i]->Text == ""){
					isFilled = false;
				}
			}

			return isFilled;
		}
		bool checkToSeeIfPuzzleIsSolved(array<System::Windows::Forms::Button^>^ buttonArray){
				int currentIndexPoint;
				bool puzzleCheckOngoing = true;
				bool puzzleSolved;
				for (int i = 0; buttonArray->Length - 1 >= i; i++){
					if (buttonArray[i]->Text == "1"){
						currentIndexPoint = i;
					}
				}

				while (puzzleCheckOngoing){


					if (buttonArray[currentIndexPoint]->Text == "64"){
						puzzleSolved = true;
						puzzleCheckOngoing = false;
					}
					else if (currentIndexPoint + 1 <= 63 && Int32::Parse(buttonArray[currentIndexPoint + 1]->Text) == Int32::Parse(buttonArray[currentIndexPoint]->Text) + 1){
						currentIndexPoint = currentIndexPoint + 1;
					}
					else if (currentIndexPoint - 1 >= 0 && Int32::Parse(buttonArray[currentIndexPoint - 1]->Text) == Int32::Parse(buttonArray[currentIndexPoint]->Text) + 1){
						currentIndexPoint = currentIndexPoint - 1;
					}
					else if (currentIndexPoint + 8 <= 63 && Int32::Parse(buttonArray[currentIndexPoint + 8]->Text) == Int32::Parse(buttonArray[currentIndexPoint]->Text) + 1){
						currentIndexPoint = currentIndexPoint + 8;
					}
					else if (currentIndexPoint - 8 >= 0 && Int32::Parse(buttonArray[currentIndexPoint - 8]->Text) == Int32::Parse(buttonArray[currentIndexPoint]->Text) + 1){
						currentIndexPoint = currentIndexPoint - 8;
					}
					else {
						puzzleSolved = false;
						puzzleCheckOngoing = false;
					}
				}

				return puzzleSolved;
			}

		void loadStartingPuzzle(String^ filename) {
			try {
				StreamReader^ streamer = File::OpenText(filename);
				String^ delimStr = ",";
				array<Char>^ delimiter = delimStr->ToCharArray();
				array<String^>^ words;
				String^ str = streamer->ReadLine();

				words = str->Split(delimiter);
				this->fakePuzzle[63] = "15";
				this->fakePuzzle[62] = "16";
				this->fakePuzzle[61] = "17";
				this->fakePuzzle[60] = "18";
				this->fakePuzzle[59] = "19";
				this->fakePuzzle[58] = "20";
				this->fakePuzzle[57] = "21";
				this->fakePuzzle[56] = "22";
				this->fakePuzzle[55] = "14";
				this->fakePuzzle[54] = "39";
				this->fakePuzzle[53] = "38";
				this->fakePuzzle[52] = "37";
				this->fakePuzzle[51] = "36";
				this->fakePuzzle[50] = "35";
				this->fakePuzzle[49] = "34";
				this->fakePuzzle[48] = "23";
				this->fakePuzzle[47] = "13";
				this->fakePuzzle[46] = "40";
				this->fakePuzzle[45] = "61";
				this->fakePuzzle[44] = "60";
				this->fakePuzzle[43] = "59";
				this->fakePuzzle[42] = "58";
				this->fakePuzzle[41] = "33";
				this->fakePuzzle[40] = "24";
				this->fakePuzzle[39] = "12";
				this->fakePuzzle[38] = "41";
				this->fakePuzzle[37] = "62";
				this->fakePuzzle[36] = "";
				this->fakePuzzle[35] = "";
				this->fakePuzzle[34] = "57";
				this->fakePuzzle[33] = "32";
				this->fakePuzzle[32] = "25";
				this->fakePuzzle[31] = "11";
				this->fakePuzzle[30] = "42";
				this->fakePuzzle[29] = "49";
				this->fakePuzzle[28] = "50";
				this->fakePuzzle[27] = "51";
				this->fakePuzzle[26] = "56";
				this->fakePuzzle[25] = "31";
				this->fakePuzzle[24] = "26";
				this->fakePuzzle[23] = "10";
				this->fakePuzzle[22] = "43";
				this->fakePuzzle[21] = "48";
				this->fakePuzzle[20] = "47";
				this->fakePuzzle[19] = "52";
				this->fakePuzzle[18] = "55";
				this->fakePuzzle[17] = "30";
				this->fakePuzzle[16] = "27";
				this->fakePuzzle[15] = "9";
				this->fakePuzzle[14] = "44";
				this->fakePuzzle[13] = "45";
				this->fakePuzzle[12] = "46";
				this->fakePuzzle[11] = "53";
				this->fakePuzzle[10] = "54";
				this->fakePuzzle[9] = "29";
				this->fakePuzzle[8] = "28";
				this->fakePuzzle[7] = "8";
				this->fakePuzzle[6] = "7";
				this->fakePuzzle[5] = "6";
				this->fakePuzzle[4] = "5";
				this->fakePuzzle[3] = "4";
				this->fakePuzzle[2] = "3";
				this->fakePuzzle[1] = "2";
				this->fakePuzzle[0] = gcnew String(words[0]);
			}
			catch (Exception^ e)
			{
				if (dynamic_cast<FileNotFoundException^>(e))
					Console::WriteLine("file '{0}' not found", filename);
				else
					Console::WriteLine("problem reading file '{0}'", filename);
			}

		}

		void controlButton(bool power) {
			for each (Button^ button in this->buttonCollection) {
				button->Enabled = power;
			}
		}

		private: System::Void button_Click(System::Object^  sender, System::EventArgs^  e) {
			if (checkLetters()){
				safe_cast<Button^>(sender)->Text = this->txtValue->Text;
				resetTextBox();
			}
		}

		private: System::Void btnReset_Click(System::Object^  sender, System::EventArgs^  e) {
			for each (Button ^button in this->buttonCollection) {
				if (button->Enabled) {
					button->Text = "";
				}
			}
		}

		private: System::Void btnEvaluate_Click(System::Object^  sender, System::EventArgs^  e) {
					 if (isPuzzleFilled(this->buttonCollection)){


						 if (checkToSeeIfPuzzleIsSolved(this->buttonCollection)){
							 MessageBox::Show(L"Correct");
							 resetTextBox();
						 }
						 else if (!checkToSeeIfPuzzleIsSolved(this->buttonCollection)){
							 MessageBox::Show(L"InCorrect");
							 resetTextBox();
						 }
					 }
					 else {
						 MessageBox::Show(L"Please fill in puzzle before evaluating");
						 resetTextBox();
					 }
		}
		
		private: System::Void btnPlay_Click(System::Object^  sender, System::EventArgs^  e) {
			this->controlButton(true);
			setUpButtonArray();
			this->loadStartingPuzzle("Files/PuzzlesA.csv");
			setButtonsToPuzzle(this->fakePuzzle);
		}
};
}
