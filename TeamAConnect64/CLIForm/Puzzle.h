#pragma once

namespace model {
	ref class Puzzle
	{
	public:
		Puzzle();
		bool checkToSeeIfPuzzleIsSolved(array<System::Windows::Forms::Button^>^);
	};
}

